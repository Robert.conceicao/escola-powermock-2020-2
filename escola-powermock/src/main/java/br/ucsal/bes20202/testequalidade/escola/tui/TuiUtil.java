package br.ucsal.bes20202.testequalidade.escola.tui;

import java.util.Scanner;

public class TuiUtil {

	private static Scanner scanner = new Scanner(System.in);

	public static String obterNomeCompleto() {
		System.out.println("Informe o nome:");
		String nome = scanner.nextLine();
		System.out.println("Informe o sobrenome:");
		String sobrenome = scanner.nextLine();
		return nome + " " + sobrenome;
	}

	public static void exibirMensagem(String mensagem) {
		System.out.print("Bom dia! ");
		System.out.println(mensagem);
	}

}
